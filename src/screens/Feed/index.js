import React, { useEffect, useState } from 'react';
import { Text, StyleSheet, SafeAreaView, FlatList } from 'react-native';
import { Divider } from 'react-native-elements';
import { BallIndicator } from 'react-native-indicators';

import { useSelector, useDispatch } from 'react-redux';
import { getusers, getfaces } from '../../redux/reducers/user/actions';
import { getposts, getcomments, createcombinedposts, deletepost, createpost } from '../../redux/reducers/feed/actions';

export const FeedScreen = () => {
	
	const users = useSelector(state => state.users);
	const feed = useSelector(state => state.feed)
	const dispatch = useDispatch();

	const getUsers = (users, getUsersFailed) => dispatch(getusers(users, getUsersFailed));
	const getFaces = (faces, getFacesFailed) => dispatch(getfaces(faces, getFacesFailed));

	const getPosts = () => dispatch(getposts());
	const getComments = () => dispatch(getcomments());
	const createCombinedPosts = (posts, comments, users) => dispatch(createcombinedposts());
	const deletePost = (postId) => dispatch(deletepost(postId));
	const createPost = (post) => dispatch(createpost(post));

	
	
	useEffect(() => {		
		users.posts ? null : getPosts();
		users.comments ? null : getComments();


	}, []);


	return (
		// (isGettingUsers || isGettingFaces ? <BallIndicator color="orange"/> : 
		<SafeAreaView>
			<Text style={styles.text}>Feed</Text>
			<Divider/>
			{/* <FlatList 
				data={createPostData}
				renderItem={(post, comments) => (
					<PostCard
						post={post}
						comments={comments}
					/>
				)}
				keyExtractor={(todo) => todo.id.toString()}
				style={{margin: 20}}
			/>				 */}
		</SafeAreaView>
	)
	// )
}


const styles = StyleSheet.create({
	text: {
		textAlign: 'center',
		marginTop: 20,
		fontSize: 20,
		marginBottom: 10
	},
})